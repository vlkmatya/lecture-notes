\documentclass[a4paper,11pt]{article}

%\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}

\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}

\usepackage{hyperref}
\usepackage{url}

\usepackage{tikz}
\usetikzlibrary{arrows}
\usetikzlibrary{backgrounds}
\usetikzlibrary{decorations.pathmorphing}
\usetikzlibrary{graphs}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{calc}
\usetikzlibrary{patterns}
\usetikzlibrary{decorations.markings}
\usepackage{lettrine}
\usepackage{titlesec}
\usepackage{fancyhdr}

%\usepackage{algorithm}
%\usepackage{algorithmic}
%\usepackage[usenames]{color}
\usepackage{listings}
%\usepackage{dtsyntax}

\usepackage{siunitx}
\usepackage[siunitx,europeanresistors]{circuitikz}

\usepackage{mgbondgraph}

%========== okraje stranky ====================================

\oddsidemargin 0.5cm 
\evensidemargin 0.5cm

\topmargin 3pt
\headheight 15pt
\headsep 20pt

\textwidth 13cm

\marginparwidth 3cm 
\marginparsep 0.5cm 

\textheight 21cm
\footskip 2cm
 
%============== nove prikazy a prostredi ============================

\newtheorem{example}{Example}[section]

%======================================================

\definecolor{cvutdarkblue}{RGB}{0,99,168}
\definecolor{cvutlighterblue}{RGB}{112,153,204}
\definecolor{cvutlightestblue}{RGB}{217,227,245}
\definecolor{light-gray}{gray}{0.75}

\lstset{language=Matlab, basicstyle=\scriptsize, keywordstyle=\color{blue}, backgroundcolor=\color{light-gray}}

%============== zahlavi a zapati ======================

\pagestyle{fancy}

\fancyhead{}
\rhead{\bfseries Introduction to bond graphs---basic principles and components}

\fancyfoot{}
\rfoot{\thepage}
\lfoot{Lecture 2 on Modeling and Simulation of Dynamic Systems}

\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

%\titleformat*{\section}{\fontfamily{pag}\fontsize{14pt}{16pt}\selectfont}
%============== opening ================================

\begin{document}

\thispagestyle{empty}

\begin{tikzpicture}[remember picture,overlay]

\coordinate [below=2.5cm] (midpoint) at (current page.north);

\node [name=colourbar,
draw=cvutlighterblue,
anchor=base,
fill=cvutlighterblue,
text = white,
minimum width=\paperwidth,
minimum height=1cm] at (midpoint) {};

% Define the point where the logo will go
\coordinate [right=20cm] (number) at (colourbar.west);

% Set coordinate system origin
\begin{scope}[shift=(number)]
% Draw the outline
\filldraw [cvutdarkblue] (1.3,0.5) -- ++(-2,0) -- ++(-0.8,-1) -- ++(2.8,0) --cycle;
% Include the logo
\node [text=white] {\Huge{\textbf{2}}};
\end{scope}

\end{tikzpicture}

%\vspace{-0.8cm}

\noindent
{\fontfamily{pag}\fontsize{31pt}{37pt}\selectfont Introduction to modeling using bond graphs}\\
\vspace{0.8cm}
{\fontfamily{pag}\fontsize{12pt}{12pt}\selectfont  Fundamental principles, basic components}\\
\vspace{0.0cm}
\begin{flushright}{\fontfamily{pag}\fontsize{12pt}{14pt}\fontshape{it}\selectfont  Zden\v ek Hur\' ak}\\\today\end{flushright}
\vspace{0.8cm}

\lettrine{I}{n} this lecture we will proceed further into the popular modeling methodology based on \textit{bond graphs}. This technique generalizes the concept of $n$-ports well known from the electrical engineering---by connecting two components or subsystems the interaction between the two is realized by an exchange of energy. The rate of transfer of energy (the power) is given as a product of two variables---voltage and current in electrical circuits, force and velocity in mechanical translation and so on. We will now develop a set of basic types of general(ized) components---generalized sources, generalized loads, generalized resistors, generalized transformers, generalized gyrators, generalized inertances and generalized compliances. Only after introducing these will we be prepared for building more complex models.

% \section{System boundary and exchange of energy}
% Having mentioned energy and power as the important variables, it cannot escape one's attention that these have been thoroughly investigated in the field of thermodynamics. And indeed, some concepts from thermodynamics will be very useful for us even when modeling mechanical, electrical or hydraulic systems. First, it is the concept of a \textit{system boundary}. The system boundary is a closed surface that encloses the system and all exchange of energy with the system surroundings (environment) goes through this surface.
% 
% Invoking again our previous example of an electric motor and a mechanical load as in Fig.~\ref{fig:motor_and_load_verbal_bond_graph_labeled}, the motor ``communicates'' with its environment through two openings in the system boundary---two bonds (or ports).
% 
% \tikzstyle{block} = [draw, fill=blue!20, rectangle,minimum height=3em, inner sep=1mm]
% \tikzstyle{input} = [coordinate, inner sep=1mm]
% \tikzstyle{output} = [coordinate, inner sep=1mm]
% \tikzstyle{sum} = [draw, fill=blue!20, circle, node distance=1.2cm, minimum size=3mm, inner sep=1mm]
% \tikzstyle{pinstyle} = [pin edge={to-,thin,black}]
% 
% \begin{figure}[ht]
%  \centering
% \begin{tikzpicture}[auto, node distance=4cm,>=latex', inner sep=1mm, minimum width=3cm]
% \node [block] (battery) {Battery};
% \node [block,right of=battery] (motor) {Motor};
% \node [block, right of=motor] (load) {Mechanical load};
% \draw (battery) to node {$u$} node[swap] {$i$} (motor);
% \draw (motor) to node {$\tau$} node [swap] {$\omega$} (load);
% \end{tikzpicture}
%  \caption{Bond graph for an interconnection motor and a load with the labels for the two associated variables.}
%  \label{fig:motor_and_load_verbal_bond_graph_labeled}
% \end{figure}  
% 
% The rate of transfer of energy is characterized by a product of two distinguished variables: electric current and electric voltage on the battery side, torque and angular velocity on the load side.


\section{Half-arrow notation in bond graphs}
Although the key information about the existence of a power bond between two subsystem or components has already been included in the bond graph, there is still one more useful information that needs to be added. This information is graphically encoded as a half-arrow as in Fig.~\ref{fig:A_B_with_half_arrow}. It is vital to understand the meaning of this new symbol correctly! It shows the direction of flow of energy if both $e$ and $\dot q$ have the same sign, that is, when $\mathcal{P}>0$; it does not suggest that the energy is flowing exclusively from A to B. The energy can easily flow from B to A. 

\begin{figure}[ht]
\centering
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (A) {A};
\node (B) [right of=A] {B};
\bond{A}{B}{$e$}{$\dot q$};
\end{tikzpicture}
\caption{Simple bond graph depicting an energy exchange (power bond) between A and B, now including also the half-arrow, which shows the direction of flow of energy if both $e$ and $\dot q$ are positive.}
\label{fig:A_B_with_half_arrow}
\end{figure}

As an example, let us again consider the tug-of-war game as in Fig.~\ref{fig:tug_of_war}

\begin{figure}[ht]
 \centering
 \includegraphics[width=8cm]{./figures/tug_of_war.png}
 % Sketch23163731.png: 1280x800 pixel, 72dpi, 45.16x28.22 cm, bb=0 0 1280 800
 \caption{Sketch of a tug-of-war game.}
 \label{fig:tug_of_war}
\end{figure}


As we have already agreed, the (generalized) force is a scalar variable here, we only need to agree that it is positive in extension (well, we can hardly experience pushing while using a rope). Say, the team B is dominating, that is, moving backward whereas the team A is yielding, that is, moving forward. What is the direction of the half-arrow? In which direction does the energy flow if the team A starts dominating, that is, moving backwards? Will then the direction of the half-arrow change?

\section{Sources and sinks}

\subsection{Ideal sources and sinks}

\begin{figure}[ht]
\centering
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (S) {$S_e$};
\node (B) [right of=S] {};
\bond{S}{B}{$e$}{$\dot q$};
\end{tikzpicture}
\caption{Ideal source of generalized effort (or force).}
\end{subfigure}
\hspace{0.05\textwidth}
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (S) {$S_f$};
\node (B) [right of=S] {};
\bond{S}{B}{$e$}{$\dot q$};
\end{tikzpicture}
\caption{Ideal source of generalized flow (or velocity).}
\end{subfigure}
\caption{Ideal sources.}
\end{figure}

\begin{figure}[ht]
\centering
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (S) {$S_e$};
\node (B) [right of=S] {};
\bond{B}{S}{$e$}{$\dot q$};
\end{tikzpicture}
\caption{Ideal sink of generalized effort (or force).}
\end{subfigure}
\hspace{0.05\textwidth}
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (S) {$S_f$};
\node (B) [right of=S] {};
\bond{B}{S}{$e$}{$\dot q$};
\end{tikzpicture}
\caption{Ideal sink of generalized flow (or velocity).}
\end{subfigure}
\caption{Ideal sinks.}
\end{figure}

\subsubsection{Examples}

\begin{itemize}
 \item mechanical: gravitational force
 \item electrical: ideal generator of electrical voltage or current
 \item hydraulic
\end{itemize}

\subsection{Nonideal sources}

\begin{figure}[ht]
\centering
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (S) {$S$};
\node (B) [right of=S] {};
\bond{S}{B}{$e$}{$\dot q$};
\end{tikzpicture}
\caption{Nonideal source.}
\end{figure}

Generally nonlinear load characterictics $e=e(\dot q)$ or $\dot q=\dot q(e)$.


\subsection{Generalized resistors}
Although one symbol ($S$) will be enough for both sources and sinks (the distinction between them done via the half-arrow), we still introduce a new symbol for generalized sinks---$R$---and will call them \textit{generalized resistors}.

g

Resistor relates $e$ and $\dot q$. Nonlinear resistor can be given by

\begin{equation}
 e = e(\dot q)
\end{equation}

or by 

\begin{equation}
 \dot q = \dot q(e).
\end{equation}

A linear resistor is given by its \textit{modulus}

\begin{equation}\boxed{
 e = R\dot q}
\end{equation}

or by 

\begin{equation}\boxed{
 \dot q = \frac{e}{R}.}
\end{equation}

\subsubsection{Examples}

\begin{itemize}
 \item mechanical: friction
 \item electrical: resistor
 \item hydraulic: viscous friction
\end{itemize}

\section{Ideal machines---generalized transformers and gyrators}

\subsection{Generalized transformers}

\begin{figure}[ht]
\centering
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (A) {};
\node (T) [right of=A] {$T$};
\node (B) [right of=T] {};
\bond{A}{T}{$e_1$}{$\dot q_1$};
\bond{T}{B}{$e_2$}{$\dot q_2$};
\end{tikzpicture}
\caption{Generalized transformer.}
\end{figure}

\begin{equation}\boxed{
 \dot q_2(t) = T \dot q_1(t)}
\end{equation}

\subsubsection{Examples}

\begin{itemize}
 \item mechanical: gears
 \item electrical: ideal transformer
 \item hydraulic: 
 \item from one domain to another---transducers: pump
\end{itemize}

\subsection{Generalized gyrators}

\begin{figure}[ht]
\centering
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (A) {};
\node (G) [right of=A] {$G$};
\node (B) [right of=G] {};
\bond{A}{G}{$e_1$}{$\dot q_1$};
\bond{G}{B}{$e_2$}{$\dot q_2$};
\end{tikzpicture}
\caption{Generalized gyrator.}
\end{figure}

\begin{equation}\boxed{
 e_2(t) = G \dot q_1(t)}
\end{equation}

\subsubsection{Examples}

\begin{itemize}
 \item mechanical: a gyroscope
 \item electrical: a circuit built from passive and active components
 \item hydraulic: ?
 \item from one domain to another---transducers: electrical DC motor with a permanent magnet
\end{itemize}

\section{Ideal accumulators of energy---generalized compliances and inertances}

\subsection{Generalized compliances}

\begin{figure}[ht]
\centering
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (A) {};
\node (C) [right of=A] {$C$};
\bond{A}{C}{$e$}{$\dot q$};
\end{tikzpicture}
\caption{Generalized compliance.}
\end{figure}


\begin{equation}\boxed{
 e(t) = \frac{1}{C} q(t)}
\end{equation}

Accumulated energy proportional to \textit{generalized displacement} $q$

\begin{equation}
 \mathcal{V} = \frac{1}{2}\frac{1}{C} q^2(t) = \frac{1}{2}C e^2(t)
\end{equation}

\subsubsection{Examples}

\begin{itemize}
 \item mechanical: spring, torsional spring
 \item electrical: capacitor
 \item hydraulic: reservoir 
\end{itemize}

\subsection{Generalized inertances}

\begin{figure}[ht]
\centering
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (A) {};
\node (I) [right of=A] {$I$};
\bond{A}{I}{$e$}{$\dot q$};
\end{tikzpicture}
\caption{Generalized inertance.}
\end{figure}

New variable introduced---generalized momentum $p(t)$---and it is defined as 

\begin{equation}
 p(t) = \int e(\tau)\mathrm{d}\tau
\end{equation}

or 

\begin{equation}
 e(t) = \dot p(t).
\end{equation}

The component called \textit{generalized inertance} is then defined as 

\begin{equation}\boxed{
 p(t) = I \dot q(t)}
\end{equation}

It accumulates an energy proportional to \textit{generalized velocity} $\dot q$.

\begin{equation}
 \mathcal{T} = \frac{1}{2}I \dot q^2(t) = \frac{1}{2}\frac{1}{I} p^2(t)
\end{equation}
\subsubsection{Examples}

\begin{itemize}
 \item mechanical: mass traveling at some velocity, mass rotating around an axis
 \item electrical: inductor
 \item hydraulic: volume of an incompressible liquid flowing at some (volumetric) flow rate.
\end{itemize}

\section{Summary}

\begin{figure}[ht]
\centering
\begin{tikzpicture}[node distance = 2.5cm]
\node (p) {$p$};
\node (s) [right of=p] {};
\node (q) [right of=s] {$q$};
\node (f) [below of=s] {$\dot q (= f)$};
\node (e) [above of=s] {$e$};
\draw (e)--node{$C$}(q);
\draw (f)--node{$\int \mathrm{d}t$, $\mathrm{d}/\mathrm{d}t$}(q);
\draw (p)--node{$I$}(f);
\draw (p)--node{$\int \mathrm{d}t$, $\mathrm{d}/\mathrm{d}t$}(e);
\draw (e)--node{$S,R$}(f);
\end{tikzpicture}
\caption{Relationship among the important (generalized) variables. Note that the horizontal connection between $p$ and $q$ has been recently (2008) invented---the memristor element.}
\end{figure}

\section{Literature}
The lecture was prepared mainly using the sections 2.1 through 3.2 in \cite{brown_engineering_2006}. There were a few important issues that we barely scratched during the lecture and you were asked to study these on your own. In particular, the topic of cascading the transformers and gyrators and the topic of matching the load to a source using a transformer, which are covered by 2.5.

As a complementary text you can use the popular \cite{karnopp_system_2012}. However, this book is probably not available in many copies in the library, if any.

The tutorial paper \cite{gawthrop_bond-graph_2007} can serve its purpose as well. You can get it from the \textit{IEEE Xplore} library within the institutional subscription. Another tutorial paper is \cite{broenink_introduction_1999}, which can be found online.

The book \cite{cellier_continuous_1991}, now available online, can also be used. However, the quality of the scanned version is poor.  

\bibliographystyle{plain}
\bibliography{bondgraphs}
 

\end{document}

\documentclass[a4paper,11pt]{article}

%\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}

\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}

\usepackage{hyperref}
\usepackage{url}

\usepackage{tikz}
\usetikzlibrary{arrows}
\usetikzlibrary{backgrounds}
\usetikzlibrary{decorations.pathmorphing}
\usetikzlibrary{graphs}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{calc}
\usetikzlibrary{patterns}
\usetikzlibrary{decorations.markings}
\usepackage{lettrine}
\usepackage{titlesec}
\usepackage{fancyhdr}

%\usepackage{algorithm}
%\usepackage{algorithmic}
%\usepackage[usenames]{color}
\usepackage{listings}
%\usepackage{dtsyntax}

\usepackage{siunitx}
\usepackage[siunitx,europeanresistors]{circuitikz}

\usepackage{mgbondgraph}

%========== okraje stranky ====================================

\oddsidemargin 0.5cm 
\evensidemargin 0.5cm

\topmargin 3pt
\headheight 15pt
\headsep 20pt

\textwidth 13cm

\marginparwidth 3cm 
\marginparsep 0.5cm 

\textheight 21cm
\footskip 2cm
 
%============== nove prikazy a prostredi ============================

\newtheorem{example}{Example}[section]

%======================================================

\definecolor{cvutdarkblue}{RGB}{0,99,168}
\definecolor{cvutlighterblue}{RGB}{112,153,204}
\definecolor{cvutlightestblue}{RGB}{217,227,245}
\definecolor{light-gray}{gray}{0.75}

\lstset{language=Matlab, basicstyle=\scriptsize, keywordstyle=\color{blue}, backgroundcolor=\color{light-gray}}

%============== zahlavi a zapati ======================

\pagestyle{fancy}

\fancyhead{}
\rhead{\bfseries Bond graphs of simple systems}

\fancyfoot{}
\rfoot{\thepage}
\lfoot{Lecture 3 on Modeling and Simulation of Dynamic Systems}

\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

%\titleformat*{\section}{\fontfamily{pag}\fontsize{14pt}{16pt}\selectfont}
%============== opening ================================

\begin{document}

\thispagestyle{empty}

\begin{tikzpicture}[remember picture,overlay]

\coordinate [below=2.5cm] (midpoint) at (current page.north);

\node [name=colourbar,
draw=cvutlighterblue,
anchor=base,
fill=cvutlighterblue,
text = white,
minimum width=\paperwidth,
minimum height=1cm] at (midpoint) {};

% Define the point where the logo will go
\coordinate [right=20cm] (number) at (colourbar.west);

% Set coordinate system origin
\begin{scope}[shift=(number)]
% Draw the outline
\filldraw [cvutdarkblue] (1.3,0.5) -- ++(-2,0) -- ++(-0.8,-1) -- ++(2.8,0) --cycle;
% Include the logo
\node [text=white] {\Huge{\textbf{3}}};
\end{scope}

\end{tikzpicture}

%\vspace{-0.8cm}

\noindent
{\fontfamily{pag}\fontsize{31pt}{37pt}\selectfont Simple bond graphs}\\
\vspace{0.8cm}
{\fontfamily{pag}\fontsize{12pt}{12pt}\selectfont  Interconnecting basic elements using junctions}\\
\vspace{0.0cm}
\begin{flushright}{\fontfamily{pag}\fontsize{12pt}{14pt}\fontshape{it}\selectfont  Zden\v ek Hur\' ak}\\\today\end{flushright}
\vspace{0.8cm}

\lettrine{W}{e} have previously introduced the concept of a (power) bond graph and some basic general(ized) elements for bond graphs. In this lecture we are going to start interconnecting these elements in order to obtain models of systems. Although some more advanced elements and techniques for bond graphs will remain to be studied, after finishing this lecture we will be perfectly ready to build mathematical models of simple yet realistic physical systems.

\section{Junctions}

In order to connect more then two elements, we use \textit{junctions}. Ideally these do not store, do not produce, do not dissipate energy. There are two variants of junctions in bond graphs.

\subsection{Type-1 junctions}

\begin{figure}[ht]
\centering
\begin{tikzpicture}[node distance = 2cm, font=\small]
\node (l) {};
\node (s) [right of=l] {$1$};
\node (r) [right of=s] {};
\node (b) [below of=s] {};
\node (a) [above of=s] {};
\bond{l}{s}{$e_1$}{$\dot q_1$};
\bond{s}{r}{$e_4$}{$\dot q_4$};
\bond{s}{a}{$e_2$}{$\dot q_2$};
\bond{s}{b}{$e_3$}{$\dot q_3$};
\end{tikzpicture}
\caption*{Type-1 junction.}
\end{figure}

Also \textit{common-flow} or \textit{common-velocity} junction

\begin{equation}\boxed{
 \dot q_1 = \dot q_2 =\dot q_3 =\dot q_4.}
\end{equation}

The conservation of energy gives

\begin{equation}
 \underbrace{e_1\dot q_1}_{\mathcal{P}_1} - \underbrace{e_2\dot q_2}_{\mathcal{P}_2} - \underbrace{e_3\dot q_3}_{\mathcal{P}_3} - \underbrace{e_4\dot q_4}_{\mathcal{P}_4} = 0,  
\end{equation}

from which it follows 

\begin{equation}
 e_1 + e_2 + e_3 - e_4 = 0.  
\end{equation}

\subsubsection{Examples}

\begin{itemize}
 \item mechanical: a single velocity and several forces
 \item electrical: common current, hence series interconnection
 \item hydraulic: common volumetric flow: pump---pipe---valve---hydraulic motor---nozzle---reservoir.
\end{itemize}

\subsection{Type-0 junctions}

\begin{figure}[ht]
\centering
\begin{tikzpicture}[node distance = 2cm, font=\small]
\node (l) {};
\node (s) [right of=l] {$0$};
\node (r) [right of=s] {};
\node (b) [below of=s] {};
\node (a) [above of=s] {};
\bond{l}{s}{$e_1$}{$\dot q_1$};
\bond{s}{r}{$e_4$}{$\dot q_4$};
\bond{s}{a}{$e_2$}{$\dot q_2$};
\bond{s}{b}{$e_3$}{$\dot q_3$};
\end{tikzpicture}
\caption*{Type-0 junction.}
\end{figure}

Also \textit{common-effort} junction

\begin{equation}\boxed{
 e_1 = e_2 = e_3 =e_4.}
\end{equation}

The conservation of energy holds identically as in the type-1 case

\begin{equation}
 \underbrace{e_1\dot q_1}_{\mathcal{P}_1} - \underbrace{e_2\dot q_2}_{\mathcal{P}_2} - \underbrace{e_3\dot q_3}_{\mathcal{P}_3} - \underbrace{e_4\dot q_4}_{\mathcal{P}_4} = 0,  
\end{equation}

from which it follows 

\begin{equation}
 \dot q_1 + \dot q_2 + \dot q_3 - \dot q_4 = 0.  
\end{equation}

\subsubsection{Examples}

\begin{itemize}
 \item mechanical: single force, several velocities
 \item electrical: common node, hence parallel interconnection
 \item hydraulic: single pressure, several nozzles.
\end{itemize}

\section{Simple systems}

\subsection{Electrical}

\begin{itemize}
 \item Identify all the distinct voltages and currents in the circuit including the directions (using arrows) for currents and polarities (using $+$ and $-$ symbols) for voltages (or voltage drops).  	 
 \item Start drawing the bond graph by putting down the type-0 junctions corresponding to the \textit{nodes} in the circuit, that is, the locations with a distinct voltage (or actually a potential).
 \item Include also the ground node. The associated bond will be erased later the related bonds because the voltage (generalized force) is zero, hence the power is zero too. But put it there just to make the modeling procedure more transparent.  
 \item Use the type-1 junctions to insert R, C and I elements that depend on differences between the nodal voltages (or actually potentials). Similarly, add the S elements (sources) between the two type-0 junctions. 
\end{itemize}

\subsection{Mechanical}

\begin{itemize}
 \item Identify all the distinct velocities in the system and highlight them in the technical drawing using arrows. Identify also all the forces. For springs and dampers, choose a polarity of the force: type either +C or +T next to the element, if the force is positive in compression or tension, respectively.
 \item Start drawing the bond graph by putting down the type-1 junctions corresponding to the velocities identified in the previous step.
 \item Include also the velocity of the reference frame. The associated bond will be erased later because the velocity is zero, hence the power is zero. But please put it there just to make the modeling procedure more transparent. After all, what is regarded as fixed now can be attached to some moving carrier in some future extension. 
 \item Use type-0 junctions to insert R and C elements that depend on the relative velocities. Attach the I elements directly to the type-1 junctions. Note that this is where the electric and mechanical worlds are fundamentally different. The mechanical instance of a generalized inertance accumulates energy proportionally to the absolute velocity. There is no point in considering relative velocity when considering a kinetic energy!
 \item Type-0 junctions can also be used together with transformers to include \textit{kinematic constraints}.
\end{itemize}


\subsection{Hydraulic}

Pretty much following the strategy for the electric systems.

\begin{itemize}
 \item Label the spots with the distinct pressures. Note the caveat, though. Unlike in low-frequency electric circuits, where we consider all energy-dissipation and energy-accumulation effects concentrated in dimensionless spots (that is also why they are called \textit{lumped systems}), in hydraulic systems such simplifying assumption is hardly possible. For example, the pressure is continuously decreasing along the pipe due to energy dissipation. Nonetheless, we at least identify the key points in the system: at the inlet, at the outlet, before the valve, after the valve, at the bottom of the reservoir and so on. Among these choose those with a distinc pressure and label it. 
 \item Similarly as in electrical systems, start drawing the bond graph by putting down the type-0 junctions that correspond to the distinct pressures identified above. 
 \item Include also the ambient pressure. Note that it has the same role as the ground in electric circuits. However, you cannot neglect it unless you switch from using the absolute pressure to the gauge pressure. It is only in this particular case that the corresponding bonds could be erased because the gauge pressure for the atmosphere will be zero.
 \item Use type-1 junctions to insert relative elements such as R, C and I. But beware that unlike in the electric circuits, here the I and R elements (corresponding to an inertia of the moving fluid and a viscous drag in the pipe) are attached to the same type-1 junction. This is one of the artefacts of the fact that we are approximating the inherently spatially distributed system (also called distributed-parameter system) with lumped elements.
\end{itemize}


\section{Literature}
This lecture was prepared using mainly the sections 3.3 through 3.5 in \cite{brown_engineering_2006}. Some more modeling strategies and examples in sections 5.1 through 5.3 of the same book. (Note that we can skip safely the chapter 4 altogether as it introduces some fundamental techniques for analysis of linear models to which you have already been exposed).

Complementary reading is in chapters 3 through 5 in the popular \cite{karnopp_system_2012}. This book is indeed a nice complementary reading. Consider getting a copy.  

Similarly as in the previous lecture which introduced the very basic concepts and compoments for bond graphs, you can consult the tutorial paper \cite{gawthrop_bond-graph_2007}. Another tutorial paper is \cite{broenink_introduction_1999}, which can be found online.

\bibliographystyle{plain}
\bibliography{bondgraphs}
 

\end{document}
